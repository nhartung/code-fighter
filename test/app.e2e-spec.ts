import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { User, UserRepository } from '../src/users/domain';

class MockedUserRepository implements UserRepository {
  private users: User[];

  constructor() {
    this.users = [];
  }

  async create(user: User): Promise<User> {
    this.users.push(user);

    return user;
  }
  async getAll(): Promise<User[]> {
    return this.users;
  }
}

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(UserRepository)
      .useClass(MockedUserRepository)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/users (GET)', () => {
    return request(app.getHttpServer())
      .get('/users')
      .expect(200)
      .expect([]);
  });

  it('creates an user with /users (POST)', async () => {
    await request(app.getHttpServer())
      .post('/users')
      .send({ name: 'code-fighter' })
      .expect(201)
      .expect({ name: 'code-fighter', care: { id: '123', name: 'test' } });

    return request(app.getHttpServer())
      .get('/users')
      .expect(200)
      .expect([{ name: 'code-fighter', care: { id: '123', name: 'test' } }]);
  });
});
