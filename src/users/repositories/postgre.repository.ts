import { User, UserRepository } from '../domain';

export class PostgreRepository implements UserRepository {
  private users: User[];
  constructor() {
    this.users = [];
  }
  async create(user: User): Promise<User> {
    const newUser = { ...user, id: `postgre-${this.users.length}` };
    this.users.push(newUser);
    return newUser;
  }
  async getAll(): Promise<User[]> {
    return this.users;
  }
}
