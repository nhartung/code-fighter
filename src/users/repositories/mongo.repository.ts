import { User, UserRepository } from '../domain';

export class MongoRepository implements UserRepository {
  private users: User[];
  constructor() {
    this.users = [];
  }
  async create(user: User): Promise<User> {
    const newUser = { ...user, id: `mongo-${this.users.length}` };
    this.users.push(newUser);
    return newUser;
  }
  async getAll(): Promise<User[]> {
    return this.users;
  }
}
