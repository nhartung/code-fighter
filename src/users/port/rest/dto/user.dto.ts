import { ApiProperty } from '@nestjs/swagger';
import { Profile } from '../../../../care/domain';
import { User } from '../../../domain';

export class UserDto extends User {
  @ApiProperty()
  id?: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  care?: Profile;
}
