import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { UserService } from '../../domain';
import { CreateUserDto, UserDto } from './dto';

@Controller('users')
@ApiTags('Users')
@UsePipes(new ValidationPipe({ transform: true }))
export class UsersController {
  constructor(@Inject(UserService) private readonly service: UserService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({ type: UserDto })
  async createUser(@Body() createUserDto: CreateUserDto): Promise<UserDto> {
    return this.service.createUser(createUserDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ type: UserDto, isArray: true })
  async getUsers(): Promise<UserDto[]> {
    return this.service.getUsers();
  }
}
