import { Module } from '@nestjs/common';
import { CareModule } from '../care/care.module';
import { UserRepository, UserService } from './domain';
import { UsersController } from './port/rest';
import { MongoRepository, PostgreRepository } from './repositories';
import { UserServiceImpl } from './services';

const makeUserRepository = (options: { useMongo: boolean }): UserRepository => {
  if (options.useMongo) {
    return new MongoRepository();
  }
  return new PostgreRepository();
};

@Module({
  controllers: [UsersController],
  imports: [CareModule],
  providers: [
    {
      provide: UserService,
      useClass: UserServiceImpl,
    },
    {
      provide: UserRepository,
      useFactory: () => makeUserRepository({ useMongo: true }),
    },
  ],
})
export class UsersModule {}
