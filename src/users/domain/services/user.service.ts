import { User } from '../entities';

export const UserService = Symbol('UserService');

export interface UserService {
  createUser(user: User): Promise<User>;
  getUsers(): Promise<User[]>;
}
