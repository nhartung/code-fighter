import { User } from '../entities';

export const UserRepository = Symbol('UserRepository');

export interface UserRepository {
  create(user: User): Promise<User>;
  getAll(): Promise<User[]>;
}
