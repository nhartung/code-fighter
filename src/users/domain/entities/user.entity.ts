import { Profile } from 'src/care/domain';

export class User {
  id?: string;
  name: string;
  care?: Profile;
}
