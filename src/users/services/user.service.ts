import { Inject, Injectable } from '@nestjs/common';
import { ProfileService } from '../../care/domain';
import { User, UserRepository, UserService } from '../domain';

@Injectable()
export class UserServiceImpl implements UserService {
  constructor(
    @Inject(UserRepository) private repository: UserRepository,
    @Inject(ProfileService) private profileService: ProfileService,
  ) {}

  async createUser(user: User): Promise<User> {
    user.care = await this.profileService.createProfile();
    return this.repository.create(user);
  }

  async getUsers(): Promise<User[]> {
    return this.repository.getAll();
  }
}
