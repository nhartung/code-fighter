import { Profile, ProfileService } from '../../care/domain';
import { User, UserService } from '../domain';
import { UserRepository } from '../domain/repositories';
import { UserServiceImpl } from './user.service';

class MockRepository implements UserRepository {
  private mocks: User[];

  constructor() {
    this.mocks = [];
  }

  async create(user: User): Promise<User> {
    this.mocks.push(user);
    return user;
  }

  async getAll(): Promise<User[]> {
    return this.mocks;
  }

  /** additional test methods */
  clear() {
    this.mocks = [];
  }

  count(): number {
    return this.mocks.length;
  }

  getMocks(): User[] {
    return this.mocks;
  }
}

const MOCKED_CARE_PROFILE = Object.freeze({
  name: 'code-fighter',
  id: 'hello-123',
});

class CareMockService extends ProfileService {
  async createProfile(): Promise<Profile> {
    return MOCKED_CARE_PROFILE;
  }
}

describe('Testing User Service', () => {
  let userService: UserService;
  const mockRepository = new MockRepository();
  const careMockService = new CareMockService();

  beforeEach(() => {
    mockRepository.clear();
    userService = new UserServiceImpl(mockRepository, careMockService);
  });

  it('Test createUser using mock repository', async () => {
    const user = { name: 'hello' };
    const createdUser = await userService.createUser(user);
    const { care } = createdUser;

    expect(createdUser.name).toEqual('hello');

    expect(care).toBeDefined();
    expect(care.id).toEqual(MOCKED_CARE_PROFILE.id);
    expect(care.name).toEqual(MOCKED_CARE_PROFILE.name);

    expect(mockRepository.count()).toEqual(1);

    const [mockUser] = mockRepository.getMocks();
    expect(createdUser).toBe(mockUser);

    const allUsers = await mockRepository.getAll();
    expect(allUsers.length).toEqual(1);
  });

  it('Test getUsers using mock repository', async () => {
    let users = await userService.getUsers();
    expect(users.length).toEqual(0);

    ['hello', 'code-fighter']
      .map((name) => ({ name }))
      .forEach((user) => {
        userService.createUser(user);
      });

    users = await userService.getUsers();
    expect(users.length).toEqual(2);
  });
});
