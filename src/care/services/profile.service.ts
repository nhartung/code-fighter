import { Injectable } from '@nestjs/common';
import { Profile, ProfileRepository, ProfileService } from '../domain';

@Injectable()
export class ProfileServiceImpl extends ProfileService {
  constructor(private repository: ProfileRepository) {
    super();
  }
  async createProfile(): Promise<Profile> {
    return this.repository.findById();
  }
}
