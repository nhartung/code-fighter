import { Module } from '@nestjs/common';
import { ProfileRepository, ProfileService } from './domain';
import { CareProfileRepository } from './repositories';
import { ProfileServiceImpl } from './services';

@Module({
  providers: [
    {
      provide: ProfileService,
      useClass: ProfileServiceImpl,
    },
    {
      provide: ProfileRepository,
      useClass: CareProfileRepository,
    },
  ],
  exports: [ProfileService],
})
export class CareModule {}
