import { Profile } from '../entities';

export abstract class ProfileRepository {
  abstract findById(): Promise<Profile>;
}
