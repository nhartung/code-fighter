import { Profile } from '../entities';

export abstract class ProfileService {
  abstract createProfile(): Promise<Profile>;
}
