import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { serverConfig } from './config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(serverConfig.prefix);

  const openAPIConfig = new DocumentBuilder()
    .setTitle('Code-Fighter')
    .setDescription('Code-Fighter presentation')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, openAPIConfig);
  SwaggerModule.setup(`${serverConfig.prefix}/api`, app, document);

  await app.listen(serverConfig.port);
  console.log(
    `visit http://localhost:${serverConfig.port}/${serverConfig.prefix}/api/`,
  );
}
bootstrap();
